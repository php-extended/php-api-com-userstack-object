<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use UnexpectedValueException;

/**
 * ApiComUserstackEndpoint class file.
 * 
 * This class acts as an endpoint for the userstack.com API.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComUserstackEndpoint implements ApiComUserstackEndpointInterface
{
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The api key.
	 * 
	 * @var ?string
	 */
	protected ?string $_apiKey;
	
	/**
	 * Whether to allow calls to http (not s) endpoint.
	 * 
	 * @var boolean
	 */
	protected bool $_allowNotHttps = false;
	
	/**
	 * Whether to use the http not secure endpoint.
	 * 
	 * @var boolean
	 */
	private bool $_forceHttp = false;
	
	/**
	 * Builds a new ApiComUserstackStringComApiEndpoint with the given processor.
	 * 
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @param ?string $apiKey
	 * @param bool $allowNoHttps
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null,
		?string $apiKey = null,
		bool $allowNoHttps = false
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		$this->_apiKey = null === $apiKey || '' === $apiKey ? null : $apiKey;
		$this->_allowNotHttps = $allowNoHttps;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComUserstack\ApiComUserstackEndpointInterface::getUserAgent()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getUserAgent(string $userAgentString) : ApiComUserstackUserAgentInterface
	{
		if(empty($userAgentString))
		{
			throw new InvalidArgumentException('The given user agent string should not be empty.');
		}
		
		if(null !== $this->_apiKey && false === $this->_forceHttp)
		{
			$url = 'https://api.userstack.com/api/detect?access_key='.\rawurlencode($this->_apiKey).'&ua='.\rawurlencode($userAgentString);
			
			try
			{
				return $this->parseApiComUserstack($url);
			}
			catch(ApiComUserstackException $ue)
			{
				switch($ue->getMessage())
				{
					case ApiComUserstackEndpointInterface::ERR_MISSING_ACCESS_KEY:
					case ApiComUserstackEndpointInterface::ERR_INVALID_ACCESS_KEY:
					case ApiComUserstackEndpointInterface::ERR_INACTIVE_USER:
					case ApiComUserstackEndpointInterface::ERR_USAGE_LIMIT_REACHED:
					case ApiComUserstackEndpointInterface::ERR_FUNCTION_ACCESS_RESTRICTED:
						// disable the api key and run on test endpoint
						$this->_apiKey = null;
						break;
					
					case ApiComUserstackEndpointInterface::ERR_HTTPS_ACCESS_RESTRICTED:
						$this->_forceHttp = true;
						break;
					
					case ApiComUserstackEndpointInterface::ERR_MISSING_USER_AGENT:
						// should not happen
						throw new RuntimeException(\strtr('The given user agent string should not be empty, wrong handling. Please send a feedback on the creator of this library (the issues url is in the composer.json at {file})', ['{file}' => \dirname(__DIR__).'/composer.json']), 501, $ue);
					
					case ApiComUserstackEndpointInterface::ERR_INVALID_API_FUNCTION:
					default:
						// should not happen
						throw new RuntimeException(\strtr('Invalid call was made by the api. Please send a feedback on the creator of this library (the issues url is in the composer.json at {file}).', ['{file}' => \dirname(__DIR__).'/composer.json']), 500, $ue);
				}
			}
		}
		
		if(null !== $this->_apiKey && $this->_allowNotHttps && $this->_forceHttp)
		{
			$url = 'http://api.userstack.com/api/detect?access_key='.\rawurlencode($this->_apiKey).'&ua='.\rawurlencode($userAgentString);
			
			try
			{
				return $this->parseApiComUserstack($url);
			}
			catch(ApiComUserstackException $ue)
			{
				switch($ue->getMessage())
				{
					case ApiComUserstackEndpointInterface::ERR_MISSING_ACCESS_KEY:
					case ApiComUserstackEndpointInterface::ERR_INVALID_ACCESS_KEY:
					case ApiComUserstackEndpointInterface::ERR_INACTIVE_USER:
					case ApiComUserstackEndpointInterface::ERR_USAGE_LIMIT_REACHED:
					case ApiComUserstackEndpointInterface::ERR_FUNCTION_ACCESS_RESTRICTED:
						// disable the api key and run on test endpoint
						$this->_apiKey = null;
						break;
					
					
					case ApiComUserstackEndpointInterface::ERR_HTTPS_ACCESS_RESTRICTED:
						// should not happen
						throw new UnexpectedValueException(\strtr('The given http call raised an https access restricted exception. Please send a feedback on the creator of this library (the issues url is in the composer.json at {file})', ['{file}' => \dirname(__DIR__).'/composer.json']), 502, $ue);
					
					case ApiComUserstackEndpointInterface::ERR_MISSING_USER_AGENT:
						// should not happen
						throw new RuntimeException(\strtr('The given user agent string should not be empty, wrong handling. Please send a feedback on the creator of this library (the issues url is in the composer.json at {file})', ['{file}' => \dirname(__DIR__).'/composer.json']), 501, $ue);
					
					case ApiComUserstackEndpointInterface::ERR_INVALID_API_FUNCTION:
					default:
						// should not happen
						throw new RuntimeException(\strtr('Invalid call was made by the api. Please send a feedback on the creator of this library (the issues url is in the composer.json at {file}).', ['{file}' => \dirname(__DIR__).'/composer.json']), 500, $ue);
				}
			}
		}
		
		// nothing worked until now, this one is far slower
		$url = 'https://userstack.com/ua_api.php?ua='.\rawurlencode($userAgentString);
		
		return $this->parseApiComUserstack($url);
	}
	
	/**
	 * Gets the user agent from the api call.
	 * 
	 * @param string $url
	 * @return ApiComUserstackUserAgentInterface
	 * @throws ApiComUserstackException if the endpoint says no
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	protected function parseApiComUserstack(string $url) : ApiComUserstackUserAgentInterface
	{
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		
		if(200 !== $response->getStatusCode())
		{
			throw new RuntimeException(\strtr('Failed to get the response from the endpoint at {url}', ['{url}' => $url]));
		}
		
		$data = $response->getBody()->__toString();
		if(empty($data))
		{
			throw new RuntimeException(\strtr('Failed to get the response data from the endpoint at {url}', ['{url}' => $url]));
		}
		
		$json = (new JsonStringDataProvider($data))->provideOne();
		if(empty($json))
		{
			throw new RuntimeException(\strtr('Failed to decode json data from the endpoint at {url}', ['{url}' => $url]));
		}
		
		if(isset($json['success']) && false === $json['success'])
		{
			if(isset($json['error']['code'], $json['error']['type'], $json['error']['info']))
			{
				throw new ApiComUserstackException((string) $json['error']['type'], (int) $json['error']['code']);
			}
		}
		
		return $this->_reifier->reify(ApiComUserstackUserAgent::class, $json);
	}
	
}
