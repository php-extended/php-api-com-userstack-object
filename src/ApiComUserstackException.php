<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;

/**
 * ApiComUserstackException class file.
 * 
 * Exception that is specific to this library.
 * 
 * @author Anastaszor
 */
class ApiComUserstackException extends RuntimeException implements ClientExceptionInterface
{
	
	// nothing to add
	
}
