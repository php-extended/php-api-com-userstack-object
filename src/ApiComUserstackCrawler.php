<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use DateTimeInterface;

/**
 * ApiComUserstackCrawler class file.
 * 
 * This is a simple implementation of the ApiComUserstackCrawlerInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUserstackCrawler implements ApiComUserstackCrawlerInterface
{
	
	/**
	 * Whether this user agent represents a crawler.
	 * 
	 * @var bool
	 */
	protected bool $_isCrawler;
	
	/**
	 * The category of the crawler.
	 * 
	 * @var string
	 */
	protected string $_category;
	
	/**
	 * The last seen date of the crawler.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_lastSeen = null;
	
	/**
	 * Constructor for ApiComUserstackCrawler with private members.
	 * 
	 * @param bool $isCrawler
	 * @param string $category
	 */
	public function __construct(bool $isCrawler, string $category)
	{
		$this->setIsCrawler($isCrawler);
		$this->setCategory($category);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets whether this user agent represents a crawler.
	 * 
	 * @param bool $isCrawler
	 * @return ApiComUserstackCrawlerInterface
	 */
	public function setIsCrawler(bool $isCrawler) : ApiComUserstackCrawlerInterface
	{
		$this->_isCrawler = $isCrawler;
		
		return $this;
	}
	
	/**
	 * Gets whether this user agent represents a crawler.
	 * 
	 * @return bool
	 */
	public function hasIsCrawler() : bool
	{
		return $this->_isCrawler;
	}
	
	/**
	 * Sets the category of the crawler.
	 * 
	 * @param string $category
	 * @return ApiComUserstackCrawlerInterface
	 */
	public function setCategory(string $category) : ApiComUserstackCrawlerInterface
	{
		$this->_category = $category;
		
		return $this;
	}
	
	/**
	 * Gets the category of the crawler.
	 * 
	 * @return string
	 */
	public function getCategory() : string
	{
		return $this->_category;
	}
	
	/**
	 * Sets the last seen date of the crawler.
	 * 
	 * @param ?DateTimeInterface $lastSeen
	 * @return ApiComUserstackCrawlerInterface
	 */
	public function setLastSeen(?DateTimeInterface $lastSeen) : ApiComUserstackCrawlerInterface
	{
		$this->_lastSeen = $lastSeen;
		
		return $this;
	}
	
	/**
	 * Gets the last seen date of the crawler.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastSeen() : ?DateTimeInterface
	{
		return $this->_lastSeen;
	}
	
}
