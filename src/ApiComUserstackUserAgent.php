<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;

/**
 * ApiComUserstackUserAgent class file.
 * 
 * This is a simple implementation of the ApiComUserstackUserAgentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiComUserstackUserAgent implements ApiComUserstackUserAgentInterface
{
	
	/**
	 * The requested user agent.
	 * 
	 * @var string
	 */
	protected string $_ua;
	
	/**
	 * The type of software.
	 * 
	 * @var ?string
	 */
	protected ?string $_type = null;
	
	/**
	 * The vendor of the software.
	 * 
	 * @var ?string
	 */
	protected ?string $_brand = null;
	
	/**
	 * The name of the software.
	 * 
	 * @var ?string
	 */
	protected ?string $_name = null;
	
	/**
	 * The url of the software.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_url = null;
	
	/**
	 * This represents the operating system information about an user agent.
	 * 
	 * @var ?ApiComUserstackOperatingSystemInterface
	 */
	protected ?ApiComUserstackOperatingSystemInterface $_os = null;
	
	/**
	 * This represents a device information about the user agent.
	 * 
	 * @var ?ApiComUserstackDeviceInterface
	 */
	protected ?ApiComUserstackDeviceInterface $_device = null;
	
	/**
	 * This represents the browser information about the user agent.
	 * 
	 * @var ?ApiComUserstackBrowserInterface
	 */
	protected ?ApiComUserstackBrowserInterface $_browser = null;
	
	/**
	 * This represents the crawler information about the user agent.
	 * 
	 * @var ?ApiComUserstackCrawlerInterface
	 */
	protected ?ApiComUserstackCrawlerInterface $_crawler = null;
	
	/**
	 * Constructor for ApiComUserstackUserAgent with private members.
	 * 
	 * @param string $ua
	 */
	public function __construct(string $ua)
	{
		$this->setUa($ua);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the requested user agent.
	 * 
	 * @param string $ua
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setUa(string $ua) : ApiComUserstackUserAgentInterface
	{
		$this->_ua = $ua;
		
		return $this;
	}
	
	/**
	 * Gets the requested user agent.
	 * 
	 * @return string
	 */
	public function getUa() : string
	{
		return $this->_ua;
	}
	
	/**
	 * Sets the type of software.
	 * 
	 * @param ?string $type
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setType(?string $type) : ApiComUserstackUserAgentInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of software.
	 * 
	 * @return ?string
	 */
	public function getType() : ?string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the vendor of the software.
	 * 
	 * @param ?string $brand
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setBrand(?string $brand) : ApiComUserstackUserAgentInterface
	{
		$this->_brand = $brand;
		
		return $this;
	}
	
	/**
	 * Gets the vendor of the software.
	 * 
	 * @return ?string
	 */
	public function getBrand() : ?string
	{
		return $this->_brand;
	}
	
	/**
	 * Sets the name of the software.
	 * 
	 * @param ?string $name
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setName(?string $name) : ApiComUserstackUserAgentInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the software.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the url of the software.
	 * 
	 * @param ?UriInterface $url
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setUrl(?UriInterface $url) : ApiComUserstackUserAgentInterface
	{
		$this->_url = $url;
		
		return $this;
	}
	
	/**
	 * Gets the url of the software.
	 * 
	 * @return ?UriInterface
	 */
	public function getUrl() : ?UriInterface
	{
		return $this->_url;
	}
	
	/**
	 * Sets this represents the operating system information about an user
	 * agent.
	 * 
	 * @param ?ApiComUserstackOperatingSystemInterface $os
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setOs(?ApiComUserstackOperatingSystemInterface $os) : ApiComUserstackUserAgentInterface
	{
		$this->_os = $os;
		
		return $this;
	}
	
	/**
	 * Gets this represents the operating system information about an user
	 * agent.
	 * 
	 * @return ?ApiComUserstackOperatingSystemInterface
	 */
	public function getOs() : ?ApiComUserstackOperatingSystemInterface
	{
		return $this->_os;
	}
	
	/**
	 * Sets this represents a device information about the user agent.
	 * 
	 * @param ?ApiComUserstackDeviceInterface $device
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setDevice(?ApiComUserstackDeviceInterface $device) : ApiComUserstackUserAgentInterface
	{
		$this->_device = $device;
		
		return $this;
	}
	
	/**
	 * Gets this represents a device information about the user agent.
	 * 
	 * @return ?ApiComUserstackDeviceInterface
	 */
	public function getDevice() : ?ApiComUserstackDeviceInterface
	{
		return $this->_device;
	}
	
	/**
	 * Sets this represents the browser information about the user agent.
	 * 
	 * @param ?ApiComUserstackBrowserInterface $browser
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setBrowser(?ApiComUserstackBrowserInterface $browser) : ApiComUserstackUserAgentInterface
	{
		$this->_browser = $browser;
		
		return $this;
	}
	
	/**
	 * Gets this represents the browser information about the user agent.
	 * 
	 * @return ?ApiComUserstackBrowserInterface
	 */
	public function getBrowser() : ?ApiComUserstackBrowserInterface
	{
		return $this->_browser;
	}
	
	/**
	 * Sets this represents the crawler information about the user agent.
	 * 
	 * @param ?ApiComUserstackCrawlerInterface $crawler
	 * @return ApiComUserstackUserAgentInterface
	 */
	public function setCrawler(?ApiComUserstackCrawlerInterface $crawler) : ApiComUserstackUserAgentInterface
	{
		$this->_crawler = $crawler;
		
		return $this;
	}
	
	/**
	 * Gets this represents the crawler information about the user agent.
	 * 
	 * @return ?ApiComUserstackCrawlerInterface
	 */
	public function getCrawler() : ?ApiComUserstackCrawlerInterface
	{
		return $this->_crawler;
	}
	
}
