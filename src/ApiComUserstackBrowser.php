<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use PhpExtended\Version\VersionInterface;

/**
 * ApiComUserstackBrowser class file.
 * 
 * This is a simple implementation of the ApiComUserstackBrowserInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUserstackBrowser implements ApiComUserstackBrowserInterface
{
	
	/**
	 * The name of the browser.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The version of the browser.
	 * 
	 * @var VersionInterface
	 */
	protected VersionInterface $_version;
	
	/**
	 * The major version of the browser.
	 * 
	 * @var int
	 */
	protected int $_versionMajor;
	
	/**
	 * The engine of the browser.
	 * 
	 * @var string
	 */
	protected string $_engine;
	
	/**
	 * Constructor for ApiComUserstackBrowser with private members.
	 * 
	 * @param string $name
	 * @param VersionInterface $version
	 * @param int $versionMajor
	 * @param string $engine
	 */
	public function __construct(string $name, VersionInterface $version, int $versionMajor, string $engine)
	{
		$this->setName($name);
		$this->setVersion($version);
		$this->setVersionMajor($versionMajor);
		$this->setEngine($engine);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the name of the browser.
	 * 
	 * @param string $name
	 * @return ApiComUserstackBrowserInterface
	 */
	public function setName(string $name) : ApiComUserstackBrowserInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the browser.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the version of the browser.
	 * 
	 * @param VersionInterface $version
	 * @return ApiComUserstackBrowserInterface
	 */
	public function setVersion(VersionInterface $version) : ApiComUserstackBrowserInterface
	{
		$this->_version = $version;
		
		return $this;
	}
	
	/**
	 * Gets the version of the browser.
	 * 
	 * @return VersionInterface
	 */
	public function getVersion() : VersionInterface
	{
		return $this->_version;
	}
	
	/**
	 * Sets the major version of the browser.
	 * 
	 * @param int $versionMajor
	 * @return ApiComUserstackBrowserInterface
	 */
	public function setVersionMajor(int $versionMajor) : ApiComUserstackBrowserInterface
	{
		$this->_versionMajor = $versionMajor;
		
		return $this;
	}
	
	/**
	 * Gets the major version of the browser.
	 * 
	 * @return int
	 */
	public function getVersionMajor() : int
	{
		return $this->_versionMajor;
	}
	
	/**
	 * Sets the engine of the browser.
	 * 
	 * @param string $engine
	 * @return ApiComUserstackBrowserInterface
	 */
	public function setEngine(string $engine) : ApiComUserstackBrowserInterface
	{
		$this->_engine = $engine;
		
		return $this;
	}
	
	/**
	 * Gets the engine of the browser.
	 * 
	 * @return string
	 */
	public function getEngine() : string
	{
		return $this->_engine;
	}
	
}
