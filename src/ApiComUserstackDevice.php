<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;

/**
 * ApiComUserstackDevice class file.
 * 
 * This is a simple implementation of the ApiComUserstackDeviceInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUserstackDevice implements ApiComUserstackDeviceInterface
{
	
	/**
	 * Whether this device is a mobile.
	 * 
	 * @var bool
	 */
	protected bool $_isMobileDevice;
	
	/**
	 * The type of device.
	 * 
	 * @var string
	 */
	protected string $_type;
	
	/**
	 * The name of the brand.
	 * 
	 * @var string
	 */
	protected string $_brand;
	
	/**
	 * The code of the brand.
	 * 
	 * @var string
	 */
	protected string $_brandCode;
	
	/**
	 * The url of the brand.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_brandUrl = null;
	
	/**
	 * The name of the device model.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiComUserstackDevice with private members.
	 * 
	 * @param bool $isMobileDevice
	 * @param string $type
	 * @param string $brand
	 * @param string $brandCode
	 * @param string $name
	 */
	public function __construct(bool $isMobileDevice, string $type, string $brand, string $brandCode, string $name)
	{
		$this->setIsMobileDevice($isMobileDevice);
		$this->setType($type);
		$this->setBrand($brand);
		$this->setBrandCode($brandCode);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets whether this device is a mobile.
	 * 
	 * @param bool $isMobileDevice
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setIsMobileDevice(bool $isMobileDevice) : ApiComUserstackDeviceInterface
	{
		$this->_isMobileDevice = $isMobileDevice;
		
		return $this;
	}
	
	/**
	 * Gets whether this device is a mobile.
	 * 
	 * @return bool
	 */
	public function hasIsMobileDevice() : bool
	{
		return $this->_isMobileDevice;
	}
	
	/**
	 * Sets the type of device.
	 * 
	 * @param string $type
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setType(string $type) : ApiComUserstackDeviceInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of device.
	 * 
	 * @return string
	 */
	public function getType() : string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the name of the brand.
	 * 
	 * @param string $brand
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setBrand(string $brand) : ApiComUserstackDeviceInterface
	{
		$this->_brand = $brand;
		
		return $this;
	}
	
	/**
	 * Gets the name of the brand.
	 * 
	 * @return string
	 */
	public function getBrand() : string
	{
		return $this->_brand;
	}
	
	/**
	 * Sets the code of the brand.
	 * 
	 * @param string $brandCode
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setBrandCode(string $brandCode) : ApiComUserstackDeviceInterface
	{
		$this->_brandCode = $brandCode;
		
		return $this;
	}
	
	/**
	 * Gets the code of the brand.
	 * 
	 * @return string
	 */
	public function getBrandCode() : string
	{
		return $this->_brandCode;
	}
	
	/**
	 * Sets the url of the brand.
	 * 
	 * @param ?UriInterface $brandUrl
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setBrandUrl(?UriInterface $brandUrl) : ApiComUserstackDeviceInterface
	{
		$this->_brandUrl = $brandUrl;
		
		return $this;
	}
	
	/**
	 * Gets the url of the brand.
	 * 
	 * @return ?UriInterface
	 */
	public function getBrandUrl() : ?UriInterface
	{
		return $this->_brandUrl;
	}
	
	/**
	 * Sets the name of the device model.
	 * 
	 * @param string $name
	 * @return ApiComUserstackDeviceInterface
	 */
	public function setName(string $name) : ApiComUserstackDeviceInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the device model.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
