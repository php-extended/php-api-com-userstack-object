<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;

/**
 * ApiComUserstackOperatingSystem class file.
 * 
 * This is a simple implementation of the
 * ApiComUserstackOperatingSystemInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUserstackOperatingSystem implements ApiComUserstackOperatingSystemInterface
{
	
	/**
	 * The name of the operating system.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The code of the operating system.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The url of presentation about the operating system.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_url;
	
	/**
	 * The family of the operating system.
	 * 
	 * @var string
	 */
	protected string $_family;
	
	/**
	 * The family code of the operating system.
	 * 
	 * @var string
	 */
	protected string $_familyCode;
	
	/**
	 * The family vendor of the operating system.
	 * 
	 * @var string
	 */
	protected string $_familyVendor;
	
	/**
	 * The icon image of the operating system.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_icon;
	
	/**
	 * The large icon image of the operating system.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_iconLarge;
	
	/**
	 * Constructor for ApiComUserstackOperatingSystem with private members.
	 * 
	 * @param string $name
	 * @param string $code
	 * @param UriInterface $url
	 * @param string $family
	 * @param string $familyCode
	 * @param string $familyVendor
	 * @param UriInterface $icon
	 * @param UriInterface $iconLarge
	 */
	public function __construct(string $name, string $code, UriInterface $url, string $family, string $familyCode, string $familyVendor, UriInterface $icon, UriInterface $iconLarge)
	{
		$this->setName($name);
		$this->setCode($code);
		$this->setUrl($url);
		$this->setFamily($family);
		$this->setFamilyCode($familyCode);
		$this->setFamilyVendor($familyVendor);
		$this->setIcon($icon);
		$this->setIconLarge($iconLarge);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the name of the operating system.
	 * 
	 * @param string $name
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setName(string $name) : ApiComUserstackOperatingSystemInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the operating system.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the code of the operating system.
	 * 
	 * @param string $code
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setCode(string $code) : ApiComUserstackOperatingSystemInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the operating system.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the url of presentation about the operating system.
	 * 
	 * @param UriInterface $url
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setUrl(UriInterface $url) : ApiComUserstackOperatingSystemInterface
	{
		$this->_url = $url;
		
		return $this;
	}
	
	/**
	 * Gets the url of presentation about the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getUrl() : UriInterface
	{
		return $this->_url;
	}
	
	/**
	 * Sets the family of the operating system.
	 * 
	 * @param string $family
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setFamily(string $family) : ApiComUserstackOperatingSystemInterface
	{
		$this->_family = $family;
		
		return $this;
	}
	
	/**
	 * Gets the family of the operating system.
	 * 
	 * @return string
	 */
	public function getFamily() : string
	{
		return $this->_family;
	}
	
	/**
	 * Sets the family code of the operating system.
	 * 
	 * @param string $familyCode
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setFamilyCode(string $familyCode) : ApiComUserstackOperatingSystemInterface
	{
		$this->_familyCode = $familyCode;
		
		return $this;
	}
	
	/**
	 * Gets the family code of the operating system.
	 * 
	 * @return string
	 */
	public function getFamilyCode() : string
	{
		return $this->_familyCode;
	}
	
	/**
	 * Sets the family vendor of the operating system.
	 * 
	 * @param string $familyVendor
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setFamilyVendor(string $familyVendor) : ApiComUserstackOperatingSystemInterface
	{
		$this->_familyVendor = $familyVendor;
		
		return $this;
	}
	
	/**
	 * Gets the family vendor of the operating system.
	 * 
	 * @return string
	 */
	public function getFamilyVendor() : string
	{
		return $this->_familyVendor;
	}
	
	/**
	 * Sets the icon image of the operating system.
	 * 
	 * @param UriInterface $icon
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setIcon(UriInterface $icon) : ApiComUserstackOperatingSystemInterface
	{
		$this->_icon = $icon;
		
		return $this;
	}
	
	/**
	 * Gets the icon image of the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getIcon() : UriInterface
	{
		return $this->_icon;
	}
	
	/**
	 * Sets the large icon image of the operating system.
	 * 
	 * @param UriInterface $iconLarge
	 * @return ApiComUserstackOperatingSystemInterface
	 */
	public function setIconLarge(UriInterface $iconLarge) : ApiComUserstackOperatingSystemInterface
	{
		$this->_iconLarge = $iconLarge;
		
		return $this;
	}
	
	/**
	 * Gets the large icon image of the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getIconLarge() : UriInterface
	{
		return $this->_iconLarge;
	}
	
}
