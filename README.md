# php-extended/php-api-com-userstack-object

A php API wrapper to connect to userstack.com API.

![coverage](https://gitlab.com/php-extended/php-api-com-userstack-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-com-userstack-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-com-userstack-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Endpoint\HttpEndpoint;
use PhpExtended\UserstackComApi\UserstackComApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */
/* @var $apiKey string */

$endpoint = new UserstackComApiEndpoint(new HttpEndpoint($client), $apiKey);

$userAgent = $endpoint->getUserAgent('<put here your user agent string (raw ascii, not encoded)>');
// $userAgent is now a \PhpExtended\UserstackComApi\UserstackComApiUserAgent

```


## License

MIT (See [license file](LICENSE)).
