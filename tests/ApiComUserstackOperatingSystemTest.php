<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack\Test;

use PhpExtended\ApiComUserstack\ApiComUserstackOperatingSystem;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackOperatingSystemTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackOperatingSystem
 * @internal
 * @small
 */
class ApiComUserstackOperatingSystemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackOperatingSystem
	 */
	protected ApiComUserstackOperatingSystem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCode());
		$expected = 'qsdfghjklm';
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetUrl() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getUrl());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setUrl($expected);
		$this->assertEquals($expected, $this->_object->getUrl());
	}
	
	public function testGetFamily() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFamily());
		$expected = 'qsdfghjklm';
		$this->_object->setFamily($expected);
		$this->assertEquals($expected, $this->_object->getFamily());
	}
	
	public function testGetFamilyCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFamilyCode());
		$expected = 'qsdfghjklm';
		$this->_object->setFamilyCode($expected);
		$this->assertEquals($expected, $this->_object->getFamilyCode());
	}
	
	public function testGetFamilyVendor() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFamilyVendor());
		$expected = 'qsdfghjklm';
		$this->_object->setFamilyVendor($expected);
		$this->assertEquals($expected, $this->_object->getFamilyVendor());
	}
	
	public function testGetIcon() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getIcon());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setIcon($expected);
		$this->assertEquals($expected, $this->_object->getIcon());
	}
	
	public function testGetIconLarge() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getIconLarge());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setIconLarge($expected);
		$this->assertEquals($expected, $this->_object->getIconLarge());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackOperatingSystem('azertyuiop', 'azertyuiop', (new UriParser())->parse('https://test.example.com'), 'azertyuiop', 'azertyuiop', 'azertyuiop', (new UriParser())->parse('https://test.example.com'), (new UriParser())->parse('https://test.example.com'));
	}
	
}
