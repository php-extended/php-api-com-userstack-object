<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack\Test;

use PhpExtended\ApiComUserstack\ApiComUserstackBrowser;
use PhpExtended\ApiComUserstack\ApiComUserstackCrawler;
use PhpExtended\ApiComUserstack\ApiComUserstackDevice;
use PhpExtended\ApiComUserstack\ApiComUserstackOperatingSystem;
use PhpExtended\ApiComUserstack\ApiComUserstackUserAgent;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackUserAgentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackUserAgent
 * @internal
 * @small
 */
class ApiComUserstackUserAgentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackUserAgent
	 */
	protected ApiComUserstackUserAgent $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetUa() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getUa());
		$expected = 'qsdfghjklm';
		$this->_object->setUa($expected);
		$this->assertEquals($expected, $this->_object->getUa());
	}
	
	public function testGetType() : void
	{
		$this->assertNull($this->_object->getType());
		$expected = 'qsdfghjklm';
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetBrand() : void
	{
		$this->assertNull($this->_object->getBrand());
		$expected = 'qsdfghjklm';
		$this->_object->setBrand($expected);
		$this->assertEquals($expected, $this->_object->getBrand());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetUrl() : void
	{
		$this->assertNull($this->_object->getUrl());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setUrl($expected);
		$this->assertEquals($expected, $this->_object->getUrl());
	}
	
	public function testGetOs() : void
	{
		$this->assertNull($this->_object->getOs());
		$expected = $this->getMockBuilder(ApiComUserstackOperatingSystem::class)->disableOriginalConstructor()->getMock();
		$this->_object->setOs($expected);
		$this->assertEquals($expected, $this->_object->getOs());
	}
	
	public function testGetDevice() : void
	{
		$this->assertNull($this->_object->getDevice());
		$expected = $this->getMockBuilder(ApiComUserstackDevice::class)->disableOriginalConstructor()->getMock();
		$this->_object->setDevice($expected);
		$this->assertEquals($expected, $this->_object->getDevice());
	}
	
	public function testGetBrowser() : void
	{
		$this->assertNull($this->_object->getBrowser());
		$expected = $this->getMockBuilder(ApiComUserstackBrowser::class)->disableOriginalConstructor()->getMock();
		$this->_object->setBrowser($expected);
		$this->assertEquals($expected, $this->_object->getBrowser());
	}
	
	public function testGetCrawler() : void
	{
		$this->assertNull($this->_object->getCrawler());
		$expected = $this->getMockBuilder(ApiComUserstackCrawler::class)->disableOriginalConstructor()->getMock();
		$this->_object->setCrawler($expected);
		$this->assertEquals($expected, $this->_object->getCrawler());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackUserAgent('azertyuiop');
	}
	
}
