<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack\Test;

use PhpExtended\ApiComUserstack\ApiComUserstackBrowser;
use PhpExtended\Version\VersionParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackBrowserTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackBrowser
 * @internal
 * @small
 */
class ApiComUserstackBrowserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackBrowser
	 */
	protected ApiComUserstackBrowser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetVersion() : void
	{
		$this->assertEquals((new VersionParser())->parse('1.0.0'), $this->_object->getVersion());
		$expected = (new VersionParser())->parse('1.2.3-beta');
		$this->_object->setVersion($expected);
		$this->assertEquals($expected, $this->_object->getVersion());
	}
	
	public function testGetVersionMajor() : void
	{
		$this->assertEquals(12, $this->_object->getVersionMajor());
		$expected = 25;
		$this->_object->setVersionMajor($expected);
		$this->assertEquals($expected, $this->_object->getVersionMajor());
	}
	
	public function testGetEngine() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getEngine());
		$expected = 'qsdfghjklm';
		$this->_object->setEngine($expected);
		$this->assertEquals($expected, $this->_object->getEngine());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackBrowser('azertyuiop', (new VersionParser())->parse('1.0.0'), 12, 'azertyuiop');
	}
	
}
