<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack\Test;

use PhpExtended\ApiComUserstack\ApiComUserstackDevice;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackDeviceTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackDevice
 * @internal
 * @small
 */
class ApiComUserstackDeviceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackDevice
	 */
	protected ApiComUserstackDevice $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testHasIsMobileDevice() : void
	{
		$this->assertFalse($this->_object->hasIsMobileDevice());
		$expected = true;
		$this->_object->setIsMobileDevice($expected);
		$this->assertTrue($this->_object->hasIsMobileDevice());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getType());
		$expected = 'qsdfghjklm';
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetBrand() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getBrand());
		$expected = 'qsdfghjklm';
		$this->_object->setBrand($expected);
		$this->assertEquals($expected, $this->_object->getBrand());
	}
	
	public function testGetBrandCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getBrandCode());
		$expected = 'qsdfghjklm';
		$this->_object->setBrandCode($expected);
		$this->assertEquals($expected, $this->_object->getBrandCode());
	}
	
	public function testGetBrandUrl() : void
	{
		$this->assertNull($this->_object->getBrandUrl());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setBrandUrl($expected);
		$this->assertEquals($expected, $this->_object->getBrandUrl());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackDevice(false, 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
