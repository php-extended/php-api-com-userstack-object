<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiComUserstack\ApiComUserstackException;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackException
 *
 * @internal
 *
 * @small
 */
class ApiComUserstackExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackException
	 */
	protected ApiComUserstackException $_object;
	
	public function testToString() : void
	{
		$this->assertInstanceOf(ApiComUserstackException::class, $this->_object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackException();
	}
	
}
