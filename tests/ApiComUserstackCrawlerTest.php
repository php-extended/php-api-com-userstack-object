<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack\Test;

use DateTimeImmutable;
use PhpExtended\ApiComUserstack\ApiComUserstackCrawler;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUserstackCrawlerTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUserstack\ApiComUserstackCrawler
 * @internal
 * @small
 */
class ApiComUserstackCrawlerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUserstackCrawler
	 */
	protected ApiComUserstackCrawler $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testHasIsCrawler() : void
	{
		$this->assertFalse($this->_object->hasIsCrawler());
		$expected = true;
		$this->_object->setIsCrawler($expected);
		$this->assertTrue($this->_object->hasIsCrawler());
	}
	
	public function testGetCategory() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCategory());
		$expected = 'qsdfghjklm';
		$this->_object->setCategory($expected);
		$this->assertEquals($expected, $this->_object->getCategory());
	}
	
	public function testGetLastSeen() : void
	{
		$this->assertNull($this->_object->getLastSeen());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setLastSeen($expected);
		$this->assertEquals($expected, $this->_object->getLastSeen());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUserstackCrawler(false, 'azertyuiop');
	}
	
}
